﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GameController : MonoBehaviour {
	public Moon[] moons;
	//public GameObject[] players;
	public GameObject player1;
	PlayerController player1Controller;
	public GameObject player2;
	PlayerController player2Controller;

	public GameObject player1Prefab;
	//will be in player select:
	public RuntimeAnimatorController player1AnimController;
	public GameObject player2Prefab;
	//will be in player select:
	public RuntimeAnimatorController player2AnimController;

	public int player1Score;
	public int player2Score;
	public float roundTime;
	public float currentRoundTime;

	public UnityEngine.UI.Text p1ScoreText;
	public UnityEngine.UI.Text p2ScoreText;
	public UnityEngine.UI.Text timeText;

	bool playing;

	public Color player1Color;
	public Color player2Color;
	public Color nullPlayerColor;

	public int[] moonsCaptured;

	public int joustPoints;
	public int capturePoints;

	public AudioClip menuMusic;
	public AudioClip gameMusic;
	public AudioClip menuSelect;

	AudioSource musicPlayer;

	//Canvas menuCanvas;
	Canvas uiCanvas;
	GameObject playerSelectMenu;
	GameObject gameMenu;

	int gameState; //switch between menu, new game, options, quit

	void Awake(){


	}

	// Use this for initialization
	void Start () {
		playing = false;
		uiCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
		musicPlayer = gameObject.GetComponent<AudioSource> ();

		playerSelectMenu = uiCanvas.transform.FindChild("PlayerSelectMenu").gameObject;
		playerSelectMenu.SetActive (false);

		gameMenu = uiCanvas.transform.FindChild("GameMenu").gameObject;

		//PlayersEnabled (false);
		moonsCaptured = new int[moons.Length];
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonUp ("escape")) {
			if(!gameMenu.activeSelf){
				LoadMenu();
			}
		}

		if (playing) {
			currentRoundTime -= Time.deltaTime;

			int seconds = Mathf.FloorToInt(currentRoundTime%60);
			string secondsString = seconds.ToString();
			if(seconds<10){
				secondsString = "0" + secondsString;
			}
			timeText.text = (Mathf.Floor(currentRoundTime/60)).ToString() + ":" + secondsString;
			if (currentRoundTime <= 0) {
				NewRound ();
			}


			int winnerPlayerNum = CheckWinCondition ();
			if (winnerPlayerNum > 0) {
				Debug.Log ("Winner is Player " + winnerPlayerNum);
			
				NewRound ();
			
				//Testing:
				//LoadMenu();
			}
			p1ScoreText.text = player1Score.ToString();
			p2ScoreText.text = player2Score.ToString();
		}
	}

	void NewPlayers(){
		//Instantiate and set player variables
		player1 = Instantiate (player1Prefab);
		player1Controller = player1.GetComponent<PlayerController> ();
		player1Controller.SetPlayerNumber (1);
		
		player2 = Instantiate (player2Prefab);
		player2Controller = player2.GetComponent<PlayerController> ();
		player2Controller.SetPlayerNumber (2);

		player1Controller.SetAnimController (player1AnimController);
		player2Controller.SetAnimController (player2AnimController);
		//Temp, set player colours on sprite
		//player1.GetComponent<SpriteRenderer>().color = player1Color;
		//player2.GetComponent<SpriteRenderer>().color = player2Color;

		//Set the other player reference
		player1Controller.SetOtherPlayer (player2);
		player2Controller.SetOtherPlayer (player1);

		PlayersEnabled (false);

		//
	}

	public Vector3 GetGravity(Vector3 position){
		Vector3 gravity = Vector3.zero;
		foreach (Moon moon in moons) {
			//gravity += (moon.transform.position - position) * moon.GetComponent<Moon>().mass;

			Vector3 r = moon.transform.position - position;
			float force = moon.GetComponent<Moon>().mass / r.sqrMagnitude;
			gravity += r * force;


		}
		//Debug.Log(gravity);
		return gravity;
	}

	int CheckWinCondition(){
		//returns the number of the winning player, 0 = no one yet
		int winningPlayerTemp = moonsCaptured[0];
		foreach (int player in moonsCaptured) {
			if( player != winningPlayerTemp){
				return 0;
			}
		}

		if (winningPlayerTemp == 1 || winningPlayerTemp == 2) {
			ScorePoints(winningPlayerTemp, 1);
		}

		return winningPlayerTemp;
	}


	void PlayersEnabled(bool state){
		//sets players to enabled/disabled
		player1.SetActive (state);
		player2.SetActive (state);
	}

	void LoadMenu(){
		//disables players and loads up the game menu
		playing = !playing;
		PlayersEnabled (false);
		gameMenu.SetActive(true);
		musicPlayer.clip = menuMusic;
		musicPlayer.Play();
	}


	public void NewGame(){
		//Called when new game button pressed
		if (player1) {
			//if we have players already in the game
			Debug.Log("Resetting Existing Players");
			PlayersEnabled (false);
			NewRound();
		} else {
			Debug.Log("No Players Yet");
			NewPlayers();

			//player1Controller.KillSelf (true);
			//player2Controller.KillSelf (true);

			PlayersEnabled (false);
		}

		//Set up scoring / timer
		player1Score = 0;
		player2Score = 0;
		currentRoundTime = roundTime;
		//

		//Spawn players
		//

		PlayersEnabled (true);

		//Start sound + music
		musicPlayer.clip = gameMusic;
		musicPlayer.Play();

		//Hide Menu
		gameMenu.SetActive(false);
        playerSelectMenu.SetActive(false);
		playing = true;
	}

	public void ScoreAType (int playerNum, string type){
		//uses the defined Joust/Capture points to score for the playerNum
		if (type == "JOUST") {
			//ScorePoints (playerNum, joustPoints);
		} else {
			if (type == "CAPTURE") {
				//ScorePoints (playerNum, capturePoints);
			}
		}
	}

	void ScorePoints (int playerNum, int points){
		//scores arbitrary points amounts for the playerNum
		if (playerNum == 1) {
			player1Score += points;
		} else {
			if (playerNum == 2) {
				player2Score += points;
			}
		}
	}

	public void Options(){
		//toggles the player select menu (game menu will have to be active first of all)
		//TODO make sure game menu is active
		playerSelectMenu.SetActive (!playerSelectMenu.activeSelf);
	}

	public bool CaptureAMoon(int playerNum, Moon capturedMoon){
		//returns true if we captured this moon successfully
		//Set the moons color, and set the array of captures
		//Debug.Log("Moons length " + moons.Length);
		for(int i = 0; i < moons.Length; i++)
		{
			//Debug.Log("Checking Moon " + i);
			if(moons[i] == capturedMoon){
				//Debug.Log("Capturing Moon");
				if (moonsCaptured[i] == playerNum){
					//this player already has the moon
					//Debug.Log("Already have this moon");
					return false;
				}
				else{
					moonsCaptured[i] = playerNum;
					//Debug.Log("Capturing Moon");
					ScoreAType (playerNum, "CAPTURE");
				
					if(playerNum == 1){
						moons[i].SetColor(player1Color);
						//return true;
					}
					else{
						if(playerNum == 2){
							moons[i].SetColor(player2Color);
							//return true;
						}
						else{
							if(playerNum == 0){
								moons[i].SetColor(nullPlayerColor);
							}
						}
					}
				}
			}
		}


		//TODO move this to update? Might avoid players planting winning flags after the round restart
		/*
		int winnerPlayerNum = CheckWinCondition ();
		if (winnerPlayerNum > 0) {
			Debug.Log("Winner is Player " + winnerPlayerNum);

			NewRound();

			//Testing:
			//LoadMenu();
		}
		*/

		return true;

	}

	void NewRound(){
		//Called when one player wins
		//Reset players / lives, respawn at home moon
		//Maintain score

		//Show a message for a few seconds of round winner

		//Reset moons
		foreach (Moon moon in moons) {
			CaptureAMoon(0, moon);
		}

		//clear flags
		GameObject[] flags = GameObject.FindGameObjectsWithTag("Flag");
		
		foreach (GameObject flag in flags) {
			Destroy(flag);
		}

		//reset round timer
		currentRoundTime = roundTime;

		//Respawn players
		player1Controller.KillSelf(true);
		player2Controller.KillSelf(true);
	}

	public GameObject GetSpawnLocation(int playerNum){
		//if playernum has a moon captured, respawn there, otherwise pick a non captured moon, otherwise pick any moon (shouldnt happen as game should have ended)
		//Moon spawnMoon = null;
		//bool foundSpawn = false;
		List<Moon> myMoonsList = new List<Moon> ();
		List<Moon> unownedMoonsList = new List<Moon> ();

		//Find My Moons
		for (int i = 0; i < moons.Length; i++) {
			if (moonsCaptured [i] == playerNum) {
				myMoonsList.Add (moons [i]);
				//Debug.Log ("Respawn owned moon");
			}
		}

		if (myMoonsList.Count == 0) {
			//No moons owned by me, Find Unowned Moons
			for (int i = 0; i < moons.Length; i++) {
				if (moonsCaptured [i] == 0) {
					unownedMoonsList.Add (moons [i]);
					//Debug.Log ("Respawn unowned moon");
				}
			}
		} else {
			//Found at least one moon owned by me
			return myMoonsList[Random.Range(0,myMoonsList.Count-1)].GetRandomSpawnLocation();
		}

		//pick any moon
		if (unownedMoonsList.Count == 0) {
			//No unowned moons found, spawn at any one
			//Debug.Log ("Respawn random moon");
			//return moons [Random.Range (0, moons.Length - 1)].GetRandomSpawnLocation ();
			return moons [Random.Range (0, moons.Length)].GetRandomSpawnLocation ();
		} else {
			//Unowned moons were found, spawn at one
			//return unownedMoonsList[Random.Range(0,unownedMoonsList.Count-1)].GetRandomSpawnLocation();
			return unownedMoonsList[Random.Range(0,unownedMoonsList.Count)].GetRandomSpawnLocation();
		}

	}

	public void Quit(){
		Application.Quit ();
	}
}
