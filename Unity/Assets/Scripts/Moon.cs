﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Moon : MonoBehaviour {
	public float mass;

	//public float spawndistance;

    public List<GameObject> spawnLocations;

	GameController gameController;

	SpriteRenderer renderer;

	// Use this for initialization
	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController>();
		renderer = transform.FindChild("Atmosphere").GetComponent<SpriteRenderer> ();

        foreach (Transform child in transform)
        {
            if(child.tag == "Spawn")
            {
                spawnLocations.Add(child.gameObject);
            }
            //spawnLocations gameObject;
        }


    }
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetColor(Color newColor){
		renderer.color = newColor;
	}

	public GameObject GetRandomSpawnLocation(){
        //should return position and rotation?
        //Vector3 spawnLocation = Vector3.zero;
        GameObject chosenLocation = spawnLocations[Random.Range(0, spawnLocations.Count-1)];
		//Vector2 spawnLocation2D = Random.insideUnitCircle.normalized;
		//Vector3 spawnLocation = Vector3.Scale(new Vector3(spawnLocation2D.x,spawnLocation2D.y,0), transform.lossyScale) + transform.position;
		return chosenLocation;
	}

	public Quaternion GetRotationAtLocation(Vector3 location){
        /*
		Transform tempTransform = new Transform();
		tempTransform.position = location;
		tempTransform.l
		*/
        //Quaternion rotation = Quaternion.LookRotation(location - transform.position, transform.TransformDirection(Vector3.up));
        Quaternion rotation = Quaternion.LookRotation(location - transform.position, transform.TransformDirection(Vector3.back));
        return new Quaternion(0, 0, rotation.z, rotation.w);
        //return rotation;
	}
}
