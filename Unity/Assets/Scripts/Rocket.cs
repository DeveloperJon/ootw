﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour {

	public Vector3 targetLocation;
	public bool travelling;

	public float speed;

	public PlayerController owner;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (travelling) {
			transform.Translate((targetLocation - transform.position) * speed * Time.deltaTime);
		}
	
	}

	void SetTarget(Vector3 target){
		targetLocation = target;
	}


}
