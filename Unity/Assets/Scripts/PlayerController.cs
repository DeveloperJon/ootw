﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	GameController gameController;
	Rigidbody2D rigidbody;
	// Use this for initialization

	public float movementInput;
	public float movementForceMultiplier;

	public float jumpForce;
	public float jumpCooldown;
	float currentJumpCooldown;

	public float rotateSpeed;


	public float joustDistance;
	public float joustForce;
	public float joustCooldown;
	float currentJoustCooldown;

	public float respawnTime;
	//float currentRespawnTime;

	public GameObject flagPrefab;

	public int playerNumber;

	public Moon moonLandedOn;

	AudioSource soundPlayer;
	public AudioClip captureSound;
	public AudioClip joustSound;
	public AudioClip respawnSound;

	Animator animator;

	GameObject otherPlayer;
	void Awake(){
		animator = GetComponent<Animator> ();
	}

	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController>();
		rigidbody = GetComponent<Rigidbody2D> ();
		soundPlayer = gameObject.GetComponent<AudioSource> ();

	}

	public void SetOtherPlayer(GameObject oP){
		otherPlayer = oP;
	}

	public void SetAnimController(RuntimeAnimatorController playerAnimController){
		animator.runtimeAnimatorController = playerAnimController;
	}

	public void SetPlayerNumber(int num){
		playerNumber = num;
	}
	
	// Update is called once per frame
	void Update () {
		//Vector3 gravity = gameController.GetComponent<GameController> ().GetGravity (transform.position);
		Vector3 gravity = gameController.GetGravity (transform.position);
		Debug.DrawLine (transform.position, transform.position + gravity); 

		if (currentJoustCooldown > 0) {
			//prevent over-jousting by holding the trigger down with a short cooldown
			currentJoustCooldown -= Time.deltaTime;
		}
		if (currentJumpCooldown > 0) {
			//prevent over-jumping by holding the trigger down with a short cooldown
			currentJumpCooldown -= Time.deltaTime;
		}
		/*
		float angle = Mathf.Atan2(Input.GetAxis ("P1RotateX"), Input.GetAxis ("P1RotateY")) * Mathf.Rad2Deg  -90;
		Quaternion newQuart = Quaternion.Euler(new Vector3(0,0 , angle));
		if (Input.GetAxis ("P1RotateX") == 0 && Input.GetAxis ("P1RotateY") == 0) {
			newQuart = transform.rotation;
		}
		transform.rotation = Quaternion.RotateTowards(transform.rotation, newQuart, rotateSpeed * Time.deltaTime);
		*/

		if (playerNumber == 1) {
            //PLAYER 1
            //Combine controller and keyboard input (clamp so we can't double our speed!)
            movementInput = Mathf.Clamp(Input.GetAxis("P1Horizontal") + Input.GetAxis("P1KeyHorizontal"),-1f,1f);

            if (movementInput != 0f){
                //moving
                if (moonLandedOn){
                    animator.SetBool("playerWalking", true);
				}
				else{
                    animator.SetBool("playerWalking", false);
				}
			}
			else{
                //not moving
				animator.SetBool("playerWalking", false);
			}

			//ROTATION
            //GameController
			float angle = Mathf.Atan2(Input.GetAxis ("P1RotateX"), Input.GetAxis ("P1RotateY")) * Mathf.Rad2Deg  -90;
			Quaternion newQuart = Quaternion.Euler(new Vector3(0,0 , angle));
			if (Input.GetAxis ("P1RotateX") == 0 && Input.GetAxis ("P1RotateY") == 0) {
				newQuart = transform.rotation;
			}
			transform.rotation = Quaternion.RotateTowards(transform.rotation, newQuart, rotateSpeed * Time.deltaTime);
            //Keyboard
            if (Input.GetAxis("P1KeyRotate") != 0f) {
                transform.Rotate(Input.GetAxis("P1KeyRotate") * Vector3.back * rotateSpeed * Time.deltaTime);
            }
            

            //JUMP
            if ((Input.GetAxis("P1Vertical")== 1 || Input.GetButtonUp("P1KeyJump")) && currentJumpCooldown <= 0) {
				if(moonLandedOn){
					Debug.Log("P1 Jump");
					currentJumpCooldown = jumpCooldown; //set cooldown
					rigidbody.AddForce (transform.up * jumpForce, ForceMode2D.Impulse);
				}
			}

            //JOUST
			if (Input.GetAxis("P1Joust")>0 || Input.GetButtonUp("P1KeyJoust"))
            {
				Debug.Log("P1 Joust");
				Joust ();
			}

            //CAPTURE
			if (Input.GetAxis("P1Vertical")== -1 || Input.GetButtonUp("P1KeyCapture")){
                Debug.Log("P1 Cap");
				Capture ();
			}

		} else {
            //PLAYER 2
            //Combine controller and keyboard input (clamp so we can't double our speed!)
            movementInput = Mathf.Clamp(Input.GetAxis("P2Horizontal") + Input.GetAxis("P2KeyHorizontal"), -1f, 1f);

            if (movementInput != 0f)
            {
                //moving
                if (moonLandedOn)
                {
                    animator.SetBool("playerWalking", true);
                }
                else {
                    animator.SetBool("playerWalking", false);
                }
            }
            else {
                //not moving
                animator.SetBool("playerWalking", false);
            }

            //ROTATION
            float angle = Mathf.Atan2(Input.GetAxis ("P2RotateX"), Input.GetAxis ("P2RotateY")) * Mathf.Rad2Deg  -90;
			Quaternion newQuart = Quaternion.Euler(new Vector3(0,0 , angle));
			if (Input.GetAxis ("P2RotateX") == 0 && Input.GetAxis ("P2RotateY") == 0) {
				newQuart = transform.rotation;
			}
			transform.rotation = Quaternion.RotateTowards(transform.rotation, newQuart, rotateSpeed * Time.deltaTime);
            //Keyboard
            if (Input.GetAxis("P2KeyRotate") != 0f)
            {
                transform.Rotate(Input.GetAxis("P2KeyRotate") * Vector3.back * rotateSpeed * Time.deltaTime);
            }

            //JUMP
            if ((Input.GetAxis("P2Vertical") == 1 || Input.GetButtonUp("P2KeyJump")) && currentJumpCooldown <= 0)
            {
                if (moonLandedOn)
                {
                    Debug.Log("P2 Jump");
                    currentJumpCooldown = jumpCooldown; //set cooldown
                    rigidbody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
                }
            }

            //JOUST
            if (Input.GetAxis("P2Joust")>0 || Input.GetButtonUp("P2KeyJoust")){
				Debug.Log("P2 Joust");
				Joust ();
			}

            //CAPTURE
			if (Input.GetAxis("P2Vertical")== -1 || Input.GetButtonUp("P2KeyCapture"))
            {
                Debug.Log("P2 Cap");
                Capture ();
			}
		}

		Vector3 movementForce = transform.right * (movementInput * movementForceMultiplier);
		Debug.DrawLine (transform.position, transform.position + movementForce, Color.blue); 

		rigidbody.AddForce (gravity + movementForce);
        //rigidbody.AddTorque (rotationForce);

	}

	void Joust(){
		if (currentJoustCooldown <= 0) {
			if (Vector3.Distance (otherPlayer.transform.position, transform.position) <= joustDistance) {
				Debug.Log ("Other Player within range");
				otherPlayer.GetComponent<Rigidbody2D> ().AddForce ((otherPlayer.transform.position - transform.position) * joustForce, ForceMode2D.Impulse);
				gameController.ScoreAType (playerNumber, "JOUST");
				animator.SetTrigger("playerJoust");
				//Sound
				soundPlayer.PlayOneShot(joustSound);
				currentJoustCooldown = joustCooldown;
			} else {
				Debug.Log ("Other Player OUT OF RANGE");
			}
		}
	}

	void Capture(){
		//Capture the moon and score points
		if (moonLandedOn) {
			//Moon capturedMoon = 
			bool captured = gameController.CaptureAMoon (playerNumber, moonLandedOn);
			if(captured){
				//plant flag animation

				//Sound
				//soundPlayer.clip = CaptureSound;
				soundPlayer.PlayOneShot(captureSound);

				//spawn flag
				//rotation will need to be from moon calculation
				GameObject flag = Instantiate(flagPrefab,transform.position,moonLandedOn.GetRotationAtLocation(transform.position)) as GameObject;
				flag.GetComponent<Flag>().SetPlayerNum(playerNumber);
			}
		}

	}

	public void KillSelf(bool respawn){
		//called by colliding with edges of world, or at end of round
		gameObject.SetActive (false);
		//gameObject.GetComponent<TrailRenderer> ().time = 0.01f;

		//Debug.Break();

		if (respawn) {
			//Invoke("Respawn();
			Invoke("Respawn", respawnTime);
		}
	}

	void Respawn(){
        GameObject tempLocation = gameController.GetSpawnLocation(playerNumber);
        transform.position = tempLocation.transform.position;
        transform.rotation = tempLocation.transform.rotation;
        gameObject.SetActive (true);
		//SOUND
		soundPlayer.PlayOneShot (respawnSound);
		//gameObject.GetComponent<TrailRenderer> ().time =2.0f;
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		//Debug.Log ("Collision");

		if(col.gameObject.tag == "Moon"){
			moonLandedOn = col.gameObject.GetComponent<Moon>();
		}
		else{
			if(col.gameObject.tag == "WorldEdge"){
				KillSelf(true);
				//Respawn();
			}
		}
	}

	void OnCollisionExit2D (Collision2D col)
	{
		//Debug.Log ("Collision");
		
		if(col.gameObject.tag == "Moon"){
			moonLandedOn = null;
		}
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, joustDistance);
	}
}
