﻿using UnityEngine;
using System.Collections;

public class Flag : MonoBehaviour {
	public Sprite redFlag;
	public Sprite greenFlag;

	SpriteRenderer renderer; 
	// Use this for initialization
	void Awake () {
		renderer = transform.GetComponent<SpriteRenderer> ();
	}
	void Start () {
		//renderer = transform.GetComponent<SpriteRenderer> ();
	}


	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetPlayerNum(int playerNumber){
		Debug.Log("flag change");
		if(playerNumber == 1){

			renderer.sprite = redFlag;
		}
		if(playerNumber == 2){
			renderer.sprite = greenFlag;
		}

	}
}
